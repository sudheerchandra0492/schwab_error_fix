import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
        strict = true,
        features = {"C:/schwab_error_fix/src/test/resources/features/test.feature"},
        plugin = {"json:C:/schwab_error_fix/target/cucumber-parallel/1.json"},
        monochrome = false,
        tags = {"@schwab"},
        glue = {"stepDefinitions"})
public class Parallel01Test {
}
