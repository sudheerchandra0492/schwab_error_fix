package utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.concurrent.TimeUnit;

/**
 * Created by sudheer chennamaneni on 03/27/2020
 */
public class DriverFactory {


    private static String cwd = System.getProperty("user.dir");
    private static String driverPath = cwd + "/src/test/resources/external/";


    private enum Browsers {
        IE,
        CHROME,
        NONE;

        /**
         * Creates browser name (IE, FIREFOX, CHROME, SAFARI)
         *
         * @return Browser instance
         */
        public static Browsers browserForName(String browser) throws IllegalArgumentException {
            for (Browsers b : values()) {
                if (b.toString().equalsIgnoreCase(browser)) {
                    return b;
                }
            }
            throw browserNotFound(browser);
        }

        private static IllegalArgumentException browserNotFound(String outcome) {
            return new IllegalArgumentException(("Invalid browser [" + outcome + "]"));
        }
    }

    /**
     * Returns current webDriver session based on browser name passed in
     *
     * @param browserInUse Browser name currently being used in the session
     * @return WebDriver instance
     */
    public static WebDriver getDriver(String browserInUse) {
        Browsers browser;
        WebDriver driver;

        if (browserInUse == null) {
            browser = Browsers.CHROME;
        } else {
            browser = Browsers.browserForName(browserInUse);
        }
        switch (browser) {
            case NONE:
                return null;
            case CHROME:
                driver = createChromeDriver();
                break;
            case IE:
            default:
                driver = createIEDriver();
                break;
        }
        addAllBrowserSetup(driver);
        return driver;
    }

    /**
     * Creates and returns instance of Chrome driver with desired capabilities
     *
     * @return Chrome driver
     */
    private static WebDriver createChromeDriver() {
            System.setProperty("webdriver.chrome.driver", driverPath + "chromedriver.exe");
            return new ChromeDriver();
    }

    /**
     * Creates and returns instance of Internet Explorer driver with desired capabilities
     *
     * @return Internet Explorer driver
     */
    private static WebDriver createIEDriver() {
        DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
        capabilities.setCapability("nativeEvents", false);
        capabilities.setCapability("ignoreZoomSetting", true);
        capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
        capabilities.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
        System.setProperty("webdriver.ie.driver", driverPath + "IEDriverServer.exe");
        return new InternetExplorerDriver();
    }

    /**
     * Sets implicit wait and page load timeout for current session
     * Also maximizes the browser window
     *
     * @param driver Current instance of webDriver
     */
    private static void addAllBrowserSetup(WebDriver driver) {
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
            driver.manage().window().maximize();
    }
}
