package utilities;

import common.Config;
import org.openqa.selenium.WebDriver;

import java.util.logging.Logger;

public class Driver {

    protected static WebDriver driver;
    private static final String BROWSER = Config.getBrowser();
    private final Logger LOGGER = Logger.getLogger(Driver.class.getName());

    public void initialize() {
        if (driver == null)
            createNewDriverInstance();
    }

    /**
     * Creates a new driver instance
     */
    private void createNewDriverInstance() {
        driver = DriverFactory.getDriver(BROWSER);
    }

    /**
     * Returns the current driver
     *
     * @return driver
     */
    public WebDriver getDriver() {
        return driver;
    }

    /**
     * Quits the current driver session
     */
    public void destroyDriver() {
        LOGGER.info("destroyDriver started");
        try {
            driver.quit();
            driver = null;
        } catch (Exception e) {
            LOGGER.info("Exception: " + e.getMessage());
        }
        LOGGER.info("destroyDriver completed");
    }


}
