package common;

/**
 * Created by sudheer chennamaneni on 03/27/2020
 */

public class Config {
    private static final String BROWSER = System.getProperty("browser", Constants.CHROME_BROWSER);

    public static String getBrowser() {
        return BROWSER;
    }


}
