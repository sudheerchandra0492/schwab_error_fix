package common;

/**
 * Created by sudheer chennamaneni on 03/27/2020
 */

public class Constants {

    public static final String CHROME_BROWSER = "CHROME";
    public static final String IE_BROWSER = "IE";
    public static final String NONE = "none";
}
